const ExtendableError = require("es6-error");
const camelcaseKeys = require("camelcase-keys");

class FormatError extends ExtendableError {
  constructor(message) {
    super(message || "Format Error");
  }
}

class NotFoundError extends ExtendableError {
  constructor(message) {
    super(message || "Resource not found.");
  }
}

class DataIntegrityError extends ExtendableError {
  constructor(message) {
    super(message || "Query returns an unexpected result.");
  }
}

const transformStripeInvoice = (stripeInvoice) => {
  const {
    id,
    number,
    hosted_invoice_url,
    invoice_pdf,
    paid,
    status,
    customer,
    customer_email,
    account_country,
    billing_reason,
    attempted,
    amount_due,
    amount_paid,
    amount_remaining,
    total,
    currency,
    period_start,
    period_end,
    created,
    lines,
  } = stripeInvoice;

  const { description, plan, period } = lines.data[0];
  const { nickname, interval, interval_count } = plan;

  const invoice = {
    stripe_invoice_id: id,
    number,
    description,
    nickname,
    hosted_invoice_url,
    interval,
    interval_count,
    invoice_pdf,
    paid,
    status,
    user: {
      email: customer_email,
      stripe_customer_id: customer,
    },

    account_country,
    billing_reason,
    attempted,

    amount_due,
    amount_paid,
    amount_remaining,
    total,
    currency,

    period_start: new Date(period.start * 1000),
    period_end: new Date(period.end * 1000),
    past_period_start: new Date(period_start * 1000),
    past_period_end: new Date(period_end * 1000),

    created_at: new Date(created * 1000),
  };

  return invoice;
};

const dateFields = [
  "cancel_at",
  "canceled_at",
  "created",
  "current_period_start",
  "current_period_end",
  "period_start",
  "period_end",
  "ended_at",
  "start",
  "start_date",
  "trial_start",
  "trial_end",
];

const normalizeStripeDate = (object) => {
  Object.keys(object).forEach((key) => {
    if (typeof object[key] === "object") normalizeStripeDate(object[key]);
    else if (dateFields.includes(key))
      object[key] = new Date(object[key] * 1000);
  });
};

const any = (object) => {
  if (!Array.isArray(object)) return [];

  return camelcaseKeys(object, { deep: true });
};

const cck = (object) => {
  return camelcaseKeys(object, { deep: true });
};

const one = (object, defaultTo = null) => {
  try {
    if (!object) throw new NotFoundError();
    if (!Array.isArray(object)) return camelcaseKeys(object, { deep: true });
    if (object.length == 0) throw new NotFoundError();
    if (object.length > 1) throw new DataIntegrityError();
    return camelcaseKeys(object[0], { deep: true });
  } catch (error) {
    if (defaultTo) return camelcaseKeys(defaultTo, { deep: true });
    throw error;
  }
};

const oneFirst = (object, defaultTo = null) => {
  try {
    if (!object) throw new NotFoundError();
    let e = object;
    if (Array.isArray(object)) {
      if (object.length == 0) throw new NotFoundError();
      if (object.length > 1) throw new DataIntegrityError();
      e = object[0];
    }

    const values = Object.values(e);
    if (values.length != 1) throw new DataIntegrityError();

    return values[0];
  } catch (error) {
    if (defaultTo) return defaultTo;
    throw error;
  }
};

const maybeOne = (object, defaultTo = null) => {
  if (!Array.isArray(object) || object.length != 1) {
    if (!Array.isArray(object) || object.length == 0) return defaultTo;
    throw new DataIntegrityError();
  }

  return camelcaseKeys(object[0], { deep: true });
};

module.exports = {
  transformStripeInvoice,
  normalizeStripeDate,
  cck,
  any,
  one,
  maybeOne,
  oneFirst,
};
