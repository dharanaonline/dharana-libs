const Stripe = require("stripe");

let stripeInstance;

module.exports.stripeManager = opts => {
  const defaults = {
    removeSecrets: true,
    secretsPath: null
  };

  const options = Object.assign({}, defaults, opts);

  return {
    before: async handler => {
      const { removeSecrets, secretsPath } = options;

      if (!secretsPath) {
        throw new Error("Secret path is required in auth0");
      }

      if (!stripeInstance) {
        stripeInstance = new Stripe(handler.context[secretsPath].secretKey);
      }

      Object.assign(handler.context, { stripe: stripeInstance });
      if (removeSecrets) {
        delete handler.context[secretsPath];
      }
    }
  };
};
