const ManagementClient = require("auth0").ManagementClient;

class auth0Manager {
  #auth0Client = null;
  #options = {
    config: {
      scope:
        "read:users update:users delete:users read:users_app_metadata update:users_app_metadata",
      tokenProvider: {
        enableCache: true,
        cacheTTLInSeconds: 30
      }
    },
    removeSecrets: true,
    secretsPath: null,
    instanceName: "auth0"
  };

  constructor(opts = {}) {
    Object.assign(this.#options, opts);
  }

  getMiddleware() {
    return {
      before: async handler => {
        const {
          config,
          removeSecrets,
          secretsPath,
          instanceName
        } = this.#options;

        if (!secretsPath) {
          throw new Error("Secret path is required in auth0");
        }

        if (!this.#auth0Client) {
          this.#auth0Client = new ManagementClient({
            ...config,
            ...handler.context[secretsPath]
          });
        }

        Object.assign(handler.context, { [instanceName]: this.#auth0Client });

        if (removeSecrets) {
          delete handler.context[secretsPath];
        }
      }
    };
  }
}

module.exports = { auth0Manager };
